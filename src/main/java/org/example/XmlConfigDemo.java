package org.example;

import lombok.extern.java.Log;
import org.example.model.Student;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Log
public class XmlConfigDemo {
    public static void main(String[] args) {
        try (var context = new ClassPathXmlApplicationContext("ioc-settings.xml")) {
            Student student = context.getBean(Student.class);
            log.info(student.toString());
        }
    }
}