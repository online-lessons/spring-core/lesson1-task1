package org.example.model;

import lombok.AllArgsConstructor;
import lombok.ToString;

import java.util.Map;

@AllArgsConstructor
@ToString
public class Student {
    private String name;
    private Map<String, Integer> subjectRating;
}
